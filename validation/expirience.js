const Validator = require('validator');
const isEmpty = require('./is_empty');

module.exports = function validateExpirienceInput(data) {
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : '';
  data.company = !isEmpty(data.company) ? data.company : '';  
  data.from = !isEmpty(data.from) ? data.from : '';  
  

  if(Validator.isEmpty(data.title)) {
    errors.title = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.company)) {
    errors.company = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.from)) {
    errors.from = 'Поле обязательно';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
};