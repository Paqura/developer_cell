const Validator = require('validator');
const isEmpty = require('./is_empty');

module.exports = function validatePostInput(data) {
  let errors = {};

  data.text = !isEmpty(data.text) ? data.text : '';

  if(Validator.isEmpty(data.text)) {
    errors.text = 'Поле обязательно';
  }

  if(!Validator.isLength(data.text,{ min: 2, max: 600 })) {
    errors.text = 'Пост может быть не короче 2 и не длинее 300 символов';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
};