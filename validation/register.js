const Validator = require('validator');
const isEmpty = require('./is_empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';  

  if(!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Имя должно быть длинной от 2 до 30 символов';
  } 

  if(Validator.isEmpty(data.name)) {
    errors.name = 'Имя обязательно';
  }  

  if(!Validator.isEmail(data.email)) {
    errors.email = 'Поле невалидно, пример: email@mail.ru';
  }

  if(Validator.isEmpty(data.email)) {
    errors.email = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.password)) {
    errors.password = 'Поле обязательно';
  }

  if(!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Пароль должен быть больше или равен 6 символам';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
};