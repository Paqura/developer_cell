const Validator = require('validator');
const isEmpty = require('./is_empty');

module.exports = function validateEducationInput(data) {
  let errors = {};

  data.school = !isEmpty(data.school) ? data.school : '';
  data.degree = !isEmpty(data.degree) ? data.degree : '';
  data.fieldsofstudy = !isEmpty(data.fieldsofstudy) ? data.fieldsofstudy : '';      
  data.from = !isEmpty(data.from) ? data.from : '';  
  

  if(Validator.isEmpty(data.school)) {
    errors.school = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.degree)) {
    errors.degree = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.fieldsofstudy)) {
    errors.fieldsofstudy = 'Поле обязательно';
  }

  if(Validator.isEmpty(data.from)) {
    errors.from = 'Поле обязательно';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
};