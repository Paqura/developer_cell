const Validator = require('validator');
const isEmpty = require('./is_empty');

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.handle = !isEmpty(data.handle) ? data.handle : '';
  data.status = !isEmpty(data.status) ? data.status : ''; 
  data.skills = !isEmpty(data.skills) ? data.skills : '';  
  
  if(!Validator.isLength(data.handle, {min: 2, max: 40})) {
    errors.handle = 'Строка должна быть длиной от 2 до 40 символов';    
  }

  if(Validator.isEmpty(data.handle))  errors.handle = 'Поле обязательно';  

  if(Validator.isEmpty(data.status))  errors.status = 'Поле обязательно'; 

  if(Validator.isEmpty(data.skills)) errors.skills = 'Поле обязательно';  
  
  if(!isEmpty(data.website)) {
    if(!Validator.isURL(data.website)) {
      errors.website = 'Невалидный url';
    }
  }

  if(!isEmpty(data.twitter)) {
    if(!Validator.isURL(data.twitter)) {
      errors.twitter = 'Невалидный url';
    }
  }

  if(!isEmpty(data.youtube)) {
    if(!Validator.isURL(data.youtube)) {
      errors.youtube = 'Невалидный url';
    }
  }

  if(!isEmpty(data.vk)) {
    if(!Validator.isURL(data.vk)) {
      errors.vk = 'Невалидный url';
    }
  }

  if(!isEmpty(data.facebook)) {
    if(!Validator.isURL(data.facebook)) {
      errors.facebook = 'Невалидный url';
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
};