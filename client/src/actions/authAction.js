import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE
} from "./types";

export const registerUser = (data, history) => async dispatch => {
  dispatch({
    type: REGISTER_USER_REQUEST
  });

  try {
    const user = await axios.post("/api/users/register", data);
    if (user) {
      dispatch({
        type: REGISTER_USER_SUCCESS,
        payload: data
      });
      history.push("/login");
    }
  } catch (err) {
    dispatch({
      type: REGISTER_USER_FAILURE,
      payload: err.response.data
    });
  }
};

export const loginUser = userData => dispatch => {
  dispatch({
    type: LOGIN_USER_REQUEST
  });

  axios
    .post("/api/users/login", userData)
    .then(res => {
      const {token} = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decoded = jwt_decode(token);
      dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: decoded
      });      
    })
    .catch(err => {
      dispatch({
        type: LOGIN_USER_FAILURE,
        payload: err.response.data
      });
    });
};

export const setCurrentUser = decoded => {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: decoded
  };
};

export const logoutUser = () => dispatch => {
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  dispatch(setCurrentUser({}))
};
