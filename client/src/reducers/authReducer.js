import isEmpty from '../validation/isEmpty';
import { 
  REGISTER_USER_REQUEST, 
  REGISTER_USER_SUCCESS, 
  REGISTER_USER_FAILURE,
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE
} from "../actions/types";

const initialState = {
  isAuthenticated: false,
  user: {},
  errors: {},
  loading: false
};

export const moduleName = 'auth';

export default function(state = initialState, action) {
  const {type, payload} = action;

  switch(type) {
    case REGISTER_USER_REQUEST: 
      return {
        ...state,
        loading: true
      }
    case REGISTER_USER_SUCCESS: 
      return {
        ...state,
        loading: false,
        user: payload
      }
    case REGISTER_USER_FAILURE:
      return {
        ...state,
        errors: payload,
        loading: false
      }    

    case LOGIN_USER_REQUEST:
      return state;
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        isAuthenticated: !isEmpty(payload),
        user: payload
      }  
    case LOGIN_USER_FAILURE:
      return {
        ...state,
        errors: payload
      }      
    default: 
      return state;
  }
};