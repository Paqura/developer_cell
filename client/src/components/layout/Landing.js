import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import propTypes from 'prop-types';
import {moduleName} from '../../reducers/authReducer';

class Landing extends Component {
  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }
  render() {
    return (
      <div className="landing">
        <div className="dark-overlay landing-inner text-light">
          <div className="container">
            <div className="row">
              <div className="col-md-12 text-center">
                <h1 className="display-3 mb-4">Социальная сеть для котокодеров</h1>
                <p className="lead">
                  {' '}
                  Создавай портфолио, кодокодер, пока человечишки спят.
                </p>
                <hr />
                <Link to="/register" className="btn btn-lg btn-info mr-2">
                  Регистрация
                </Link>
                <Link to="/login" className="btn btn-lg btn-light">
                  Войти
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Landing.propTypes = {
  auth: propTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state[moduleName]
})

export default connect(mapStateToProps, null)(withRouter(Landing));
