import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { logoutUser } from "../../actions/authAction";
import { moduleName } from "../../reducers/authReducer";

const AvatarWrapperStyle = {
  display: 'flex',
  alignItems: 'center'
};

const AvatarStyle = {
  width: '25px',
  marginRight: '8px'
}

class Navbar extends Component {
  
  onLogoutUser = (e) => {
    e.preventDefault();
    this.props.logoutUser();
  }

  render() {
    const { user, isAuthenticated } = this.props.auth;
    const authLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <a 
            className="nav-link" 
            style={AvatarWrapperStyle}
            href="jsx-a11y/href-no-hash" 
            onClick={this.onLogoutUser}
          >
          <img 
              className="rounded-circle"
              src={user.avatar} 
              alt={user.name} 
              style={AvatarStyle}
              />
            Выйти
          </a>
        </li>       
      </ul>
    );
    const guestLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/register">
            Регистрация
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/login">
            Войти
          </Link>
        </li>
      </ul>
    );
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
        <div className="container">
          <Link className="navbar-brand" to="/">
            Catscell
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#mobile-nav"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="mobile-nav">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/profiles">
                  {" "}
                  Котокодеры
                </Link>
              </li>
            </ul>
            {isAuthenticated ? authLinks : guestLinks}
          </div>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  logoutUser: propTypes.func.isRequired,
  auth: propTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state[moduleName]
});

const mapDispatchToProps = {
  logoutUser
};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
