const express  = require('express');
      router   = express.Router(),
      gravatar = require('gravatar'),
      bcrypt   = require('bcryptjs'),
      jwt      = require('jsonwebtoken');
      keys     = require('../../config/keys'),
      passport = require('passport'),
      STATUS = require('../../config/status');

/**
 * * Load input validation
 */

const validateRegisterInput = require('../../validation/register'); 
const validateLoginInput = require('../../validation/login'); 


/**
 * * connect user model
 */

const User = require('../../models/User');

/**
 * * @route  GET /api/users/test
 * * @desc   тестовый роут
 * * @access Public
 */

router.get('/test', (req, res) => {
  res.json({
    msg: 'Users works'
  });
});

/**
 * * @route  GET /api/users/register
 * * @desc   регистрация юзера
 * * @access Public
 */

router.post('/register', (req, res) => {
  const {errors, isValid} = validateRegisterInput(req.body);
  
  /**
   * * check validation
   */

  if(!isValid) {
    return res.status(STATUS.BAD_REQUEST).json(errors);    
  }

  User.findOne({ email: req.body.email })
    .then(user => {
      if(user) {
        return res.status(STATUS.BAD_REQUEST).json({
          email: 'Эта почта уже зарегестрирована'  
        });
      } else {
        const avatar = gravatar.url(req.body.email, {
          s: '200', // Size,
          r: 'pg', // rating
          d: 'mm' // default
        });
        const newUser = new User({
          name:     req.body.name,
          email:    req.body.email,
          avatar,
          password: req.body.password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;

            newUser
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err));
          });
        });
      }
    })
});

/**
 * * @route  GET /api/users/login
 * * @desc   вход юзера / возвращает jwt
 * * @access Public
 */

router.post('/login', (req, res) => {
  const {errors, isValid} = validateLoginInput(req.body);
  
  /**
   * * check validation
   */

  if(!isValid) {
    return res.status(STATUS.BAD_REQUEST).json(errors);    
  }

  const email    = req.body.email,
        password = req.body.password;

  /** 
  * * Поиск юзера по email
  */      

  User.findOne({email})
    .then(user => {
      if(!user) {
        errors.email = 'Пользователя не существует'
        return res.status(STATUS.NOT_FOUND)
          .json(errors);
      } 

    bcrypt
      .compare(password, user.password)
      .then(isMatch => {
        if(isMatch) {
          /**
           * * create JWT payload
           */
          const payload = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };

          jwt.sign(
            payload, 
            keys.secretOrKey, 
            { expiresIn: 3600 }, 
            (err, token) => {
              if(err) throw err;
              res.json({
                success: true,
                token: 'Bearer ' + token
              })
            });
        } else {
          errors.password = 'Некорректный пароль'
          return res.status(STATUS.BAD_REQUEST)
            .json(errors)
        }
      })
    })
});

/**
 * * @route  GET /api/users/current
 * * @desc   возвращает текущего юзера
 * * @access Private
 */

router.get(
  '/current', 
  passport.authenticate('jwt', { session: false }), 
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email
    });
}); 

module.exports = router;