const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const STATUS = require('../../config/status');

/**
 * * Load validation
 */

const validateProfileInput = require('../../validation/profile');
const validateExpirienceInput = require('../../validation/expirience');
const validateEducationInput = require('../../validation/education');


/**
 * * Load models
 */

const Profile = require('../../models/Profile');
const User = require('../../models/User');


/**
 * * @route  GET /api/profile/test
 * * @desc   тестовый
 * * @access Public
 */

router.get('/test', (req, res) => res.json({  msg: 'profile works' }));

/**
 * * @route  GET /api/profile
 * * @desc   get current user profile
 * * @access Private
 */

router.get(
  '/', 
  passport.authenticate('jwt', { session: false }), 
  (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
      .populate('user', ['name', 'avatar'])
      .then(profile => {
        if(!profile) {
          errors.noprofile = 'Этот пользователь не имеет профиля';
          return res.status(STATUS.NOT_FOUND).json(errors)
        }
        res.json(profile);
      })
      .catch(err => res.status(STATUS.NOT_FOUND).json(err));
  }
);

/**
 * * @route  GET /api/profile/all
 * * @desc   get all profiles
 * * @access Public
 */

router.get('/all', (req, res) => {
  const errors = {};
  Profile.find()
    .populate('user', ['name', 'avatar'])
    .then(profiles => {
      if(!profiles) {
        errors.noprofile = 'Нет профилей';
        return res.status(STATUS.NOT_FOUND).json(errors);
      }
      res.json(profiles);
    })
    .catch(err => {
      res.status(STATUS.NOT_FOUND).json({profile: 'Нет профилей'});
    })
});

/**
 * * @route  GET /api/profile/handle/:handle
 * * @desc   get profile by handle
 * * @access Public
 */

router.get('/handle/:handle', (req, res) => {
  const errors = {};

  Profile.findOne({ handle: req.params.handle })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if(!profile) {
        errors.noprofile = 'У этого юзера нет профиля';
        res.status(STATUS.NOT_FOUND).json(errors);
      }
      res.json(profile);
    })
    .catch(err => res.status(STATUS.NOT_FOUND).json(err));
});

/**
 * * @route  GET /api/profile/user/:user_id
 * * @desc   get profile by userID
 * * @access Public
 */

router.get('/user/:user_id', (req, res) => {
  const errors = {};

  Profile.findOne({ handle: req.params.user_id })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if(!profile) {
        errors.noprofile = 'У этого юзера нет профиля';
        res.status(STATUS.NOT_FOUND).json(errors);
      }
      res.json(profile);
    })
    .catch(err => res.status(STATUS.NOT_FOUND).json({
      profile: 'У этого юзера нет профиля'
    }));
});

/**
 * * @route  POST /api/profile/
 * * @desc   create user profile
 * * @access Private
 */

router.post(
  '/', 
  passport.authenticate('jwt', { session: false }), 
  (req, res) => {

    const {errors, isValid} = validateProfileInput(req.body);
    
    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }
    

    /**
     * * Get field
     */

    const profileFields = {};
    profileFields.user = req.user.id;

    if(req.body.handle) profileFields.handle = req.body.handle;
    if(req.body.company) profileFields.company = req.body.company;
    if(req.body.website) profileFields.website = req.body.website;
    if(req.body.location) profileFields.location = req.body.location;
    if(req.body.bio) profileFields.bio = req.body.bio;
    if(req.body.status) profileFields.status = req.body.status;
    if(req.body.githubusername) profileFields.githubusername = req.body.githubusername;
    if(typeof req.body.skills !== 'undefined') {
      profileFields.skills = req.body.skills.split(',');
    } 

    // * Social

    profileFields.social = {};

    if(req.body.youtube) profileFields.social.youtube = req.body.youtube;
    if(req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if(req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if(req.body.vk) profileFields.social.vk = req.body.vk;  

    Profile.findOne({user: req.user.id})
      .then(profile => {
        if(profile) {
          // * Update
          Profile.findOneAndUpdate(
            { user: req.user.id },
            { $set: profileFields }, 
            { new: true}
          ).then(profile => res.json(profile));
        } else {
          // * Create
          // * Check if handle exists
          Profile.findOne({handle: profileFields.handle})
            .then(profile => {
              if(profile) {
                errors.handle = 'Этот профиль уже существует';
                res.status(STATUS.BAD_REQUEST).json(errors);
              }

              // * Save profile
              new Profile(profileFields)
                .save()
                .then(profile => res.json(profile));
            })
        }
      })
  }
);

/**
 * * @route  POST /api/profile/experience
 * * @desc   add experience to profile
 * * @access Private
 */

router.post(
  '/experience', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validateExpirienceInput(req.body);
    
    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }   


    Profile.findOne({user: req.user.id})
      .then(profile => {
        const newExp = {
          title: req.body.title,
          company: req.body.company,
          location: req.body.location,
          from: req.body.from,
          to: req.body.to,
          current: req.body.current,
          description: req.body.description
        };

        // * Add to exp array

        profile.expirience.unshift(newExp);

        profile
          .save()
          .then(profile => res.json(profile))
      })
  }
);

/**
 * * @route  POST /api/profile/education
 * * @desc   add education to profile
 * * @access Private
 */

router.post(
  '/education', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validateEducationInput(req.body);
    
    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }   


    Profile.findOne({user: req.user.id})
      .then(profile => {
        const newEdu = {
          school: req.body.school,
          degree: req.body.degree,
          fieldsofstudy: req.body.fieldsofstudy,
          from: req.body.from,
          to: req.body.to,
          current: req.body.current,
          description: req.body.description
        };

        // * Add to edu array

        profile.education.unshift(newEdu);

        profile
          .save()
          .then(profile => res.json(profile))
      })
  }
);

/**
 * * @route  DELETE /api/profile/experience:/exp_id
 * * @desc   delete experience from profile
 * * @access Private
 */

router.delete(
  '/experience/:exp_id', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {

    Profile.findOne({user: req.user.id})
      .then(profile => {
        // * Ищем порядковый номер
        const removeIndex = profile.expirience
          .map(it => item.id)
          .indexOf(req.params.exp_id);

        // * Вырезаем его
        profile.expirience.splice(removeIndex, 1);
        
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(STATUS.NOT_FOUND).json(err));
  }
);


/**
 * * @route  DELETE /api/profile/education:/exp_id
 * * @desc   delete education from profile
 * * @access Private
 */

router.delete(
  '/education/:edu_id', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {

    Profile.findOne({user: req.user.id})
      .then(profile => {
        const removeIndex = profile.education
          .map(it => item.id)
          .indexOf(req.params.edu_id);

        profile.education.splice(removeIndex, 1);
        
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(STATUS.NOT_FOUND).json(err));
  }
);

/**
 * * @route  DELETE /api/profile
 * * @desc   delete user and profile
 * * @access Private
 */

router.delete(
  '/', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    Profile.findOneAndRemove({user: req.user.id})
      .then(() => {
        User.findOneAndRemove({_id: req.user.id})
          .then(() => {
            res.json({success: true})
          })
      })
  }
) 

module.exports = router;