const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const STATUS = require('../../config/status');

/**
 * * Load Model
 */

const Post = require('../../models/Post');
const Profile = require('../../models/Profile');

/**
 * * Validation
 */

const validatePostInput = require('../../validation/post'); 

/**
 * * @route  GET /api/posts/test
 * * @desc   test
 * * @access Public
 */

router.get('/test', (req, res) => res.json({ msg: 'posts works'}));


/**
 * * @route  GET /api/posts
 * * @desc   get posts
 * * @access Public
 */

router.get('/', (req, res) => {
  Post.find()
    .sort({date: -1})
    .then(posts => res.json(posts))
    .catch(err => res.status(STATUS.NOT_FOUND).json({ nopostfound: 'Ни одного поста не найдено' }));
}); 


/**
 * * @route  GET /api/posts/:id
 * * @desc   get posts by id
 * * @access Public
 */

router.get('/:id', (req, res) => {
  Post.findById(req.params.id)
    .then(post => res.json(post))
    .catch(err => res.status(STATUS.NOT_FOUND).json({ nopostfound: 'Ни одного поста не найдено' }));
}); 

/**
 * * @route  POST /api/posts
 * * @desc   create post
 * * @access Private
 */

router.post(
  '/', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validatePostInput(req.body);

    // * check validation

    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }

    const newPost = new Post({
      text: req.body.text,
      name: req.body.name,
      avatar: req.body.avatar,
      user: req.user.id
    });

    newPost.save().then(post => res.json(post));
  }
);

/**
 * * @route  POST /api/posts/like/:id
 * * @desc   like post
 * * @access Private
 */

router.post(
  '/like/:id', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {       
    Profile.findOne({user: req.user.id})
      .then(profile => {
          Post.findById(req.params.id)
            .then(post => {
              if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
                return res.status(STATUS.BAD_REQUEST).json({ alreadyliked: 'Этот юзер уже лайкал этот пост' })
              }

              post.likes.unshift({ user: req.user.id });
              post.save().then(post => res.json(post));
            })
            .catch(err => res.status(STATUS.NOT_FOUND))
      })
  }
);

/**
 * * @route  POST /api/posts/unlike/:id
 * * @desc   unlike post
 * * @access Private
 */

router.post(
  '/unlike/:id', 
  passport.authenticate('jwt', {session: false}),
  (req, res) => {       
    Profile.findOne({user: req.user.id})
      .then(profile => {
          Post.findById(req.params.id)
            .then(post => {
              if(post.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
                return res.status(STATUS.BAD_REQUEST).json({ alreadyliked: 'Больше нельзя лайкать' })
              }

              const removeIndex = post.likes
                .map(it => it.user.toString())
                .indexOf(req.user.id);
              
              post.likes.splice(removeIndex, 1);  

              post.save().then(post => res.json(post));
            })
            .catch(err => res.status(STATUS.NOT_FOUND))
      })
  }
);

/**
 * * @route  POST /api/posts/comment/:id
 * * @desc   add comment 
 * * @access Private
 */

router.post(
  '/comment/:id',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validatePostInput(req.body);

    // * check validation

    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }

    Post.findById(req.params.id)
      .then(post => {
        const newComment = {
          text: req.body.text,
          name: req.body.name,
          avatar: req.body.avatar,
          user: req.user.id
        };

        post.comments.unshift(newComment);
        post.save().then(post => res.json(post));
      })
      .catch(err => res.status(STATUS.NOT_FOUND).json({ postnotfound: 'Пост не найден' }))    
  }
);


/**
 * * @route  DELETE /api/posts/comment/:id/:comment_id
 * * @desc   delete comment 
 * * @access Private
 */

router.delete(
  '/comment/:id/:comment_id',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validatePostInput(req.body);

    // * check validation

    if(!isValid) {
      return res.status(STATUS.BAD_REQUEST).json(errors);
    }

    Post.findById(req.params.id)
      .then(post => {
        if( 
            post.comments
            .filter(
              comment => comment._id.toString() === req.params.comment_id
            ).length === 0            
          ) {
            return res.status(STATUS.NOT_FOUND).json({ commentnotexists: 'Комментария не существует' })
          }
          const removeIndex = post.comments
            .map(comment => comment._id.toString())
            .indexOf(req.params.comment_id);
          
          post.comments.splice(removeIndex, 1);
          post.save().then(post => res.json(post));
      })
      .catch(err => res.status(STATUS.NOT_FOUND).json({ postnotfound: 'Пост не найден' }))    
  }
);

/**
 * * @route  DELETE /api/posts/:id
 * * @desc   delete post 
 * * @access Private
 */

router.delete(
  '/:id',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    Profile.findOne({user: req.user.id})
      .then(profile => {
        Post.findById(req.params.id)
          .then(post => {
            // * Check for post owner
            if(post.user.toString() !== req.user.id) {
              return res.status(STATUS.UNAUTHORIZED).json({ notauthorized: 'Юзер не авторизован' });
            }
            post.remove().then(() => res.json({succes: true}))
          })
          .catch(err => res.status(STATUS.NOT_FOUND).json({postnotfound: 'Пост не найден'}));
      });
  }
);

module.exports = router;