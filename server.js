const express    = require('express'),
      mongoose   = require('mongoose'),
      bodyParser = require('body-parser');
      passport   = require('passport');

/**
 * * add routes
 */

const users   = require('./routes/api/users'),
      profile = require('./routes/api/profile'),
      posts   = require('./routes/api/posts');


const app = express(),
      port = process.env.PORT || 8080;

/**
 * * BodyParser Middleware
 */

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/**
 * * DB Config
 */

const db = require('./config/keys').mongoURI; 

/**
 * * Connect to MongoDB
 */

mongoose.connect(db)
  .then(() => console.log('MongoDB connected'))
  .catch(err => console.log(err)); 

/**
 * * Passport middleware
 */

app.use(passport.initialize());

// * passport config

require('./config/passport')(passport);

/**
 * * use routes
 */

app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts);


app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
